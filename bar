#! /bin/sh
export POSIXLY_CORRECT=1

battery_usage() {
	# clear array
	set --

	# check for devices
	cd /sys/class/power_supply/

	# add to list
	set -- *

	# if no devices found, set variable to "N/A" and return
	if test "$*" = "*"; then
		set -- 
		var_bat="N/A"
		return 0
	fi

	# save array size with devices inside
	# ex: $ set -- a b c
	dev_size="$#"

	# save name + battery to array
	# ex: $ set -- a b c "a 100" "b 75" "c 7"
	for i in "$@"; do
		cd "$i"
		unset name
		name=$(echo "$i" | head -c 2)
		set "$@" "${name}: $(cat capacity)"
	done
	
	# remove old values in array
	# ex: $ set -- "a 100" "b 75" "c 7"
	shift "$(($# - $dev_size))"
	export var_bat="$@"
}

disk_usage() {
	export var_disk="$(
		df -h | grep -i "/$" | awk '{printf $3}'
	)"
}

get_idle_total() {
	# add cpu values to array
	set -- $(head -n1 "/proc/stat")

	# remove word "cpu"
	shift 1

	# combine all the values into cpu total pool
	for i in "$@"; do
		total="$(($total + $i))"
	done

	# store idle cpu usage
	idle="$4"
}

cpu_usage() {
	unset idle total
	get_idle_total
	idle1="$idle"
	total1="$total"

	sleep 1

	unset idle total
	get_idle_total
	idle2="$idle"
	total2="$total"

	idle3="$(($idle2 - $idle1))"
	total3="$(($total2 - $total1))"
	idle3="${idle3#-}"
	total="${total3#-}"
	export var_cpu=$(
		echo "$total3 $idle3" | awk '{ diff = $1 - $2; result = diff * 100 / $2; print int(result) }'
	)
}

memory_usage() {
	export var_mem=$(
		free --mega | grep -i "^Mem:" | awk '{print $3}'
	)
}

date_time() {
	export var_date=$(
		date "+󰃭 %a %d %b   %H:%M:%S"
	)
}

while true; do
	battery_usage
	disk_usage
	cpu_usage
	memory_usage
	date_time

	printf "  󱊣 ${var_bat}"
	printf "   ${var_cpu}%%"
	printf "   ${var_mem}MB"
	printf "  󰆓 ${var_disk}"
	printf "  ${var_date}"
	printf "\n"
done
